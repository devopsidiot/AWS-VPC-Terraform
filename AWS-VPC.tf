#Sets region
provider "aws" {
  profile = "default"
  region  = "us-east-1"
}

#Create VPC
resource "aws_vpc" "version1" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_hostnames = true

  tags = {
    Name = "VPC-v1"
  }
}

#Creates subnets within previously created VPC
resource "aws_subnet" "version1-subnet-a" {
  vpc_id            = "${aws_vpc.version1.id}"
  cidr_block        = "10.0.1.0/24"
  availability_zone = "us-east-1a"

  tags = {
    Name = "VPC-Subnet1"
  }
}

resource "aws_subnet" "version1-subnet-b" {
  vpc_id            = "${aws_vpc.version1.id}"
  cidr_block        = "10.0.2.0/24"
  availability_zone = "us-east-1b"

  tags = {
    Name = "VPC-Subnet2"
  }
}

resource "aws_subnet" "version1-subnet-c" {
  vpc_id            = "${aws_vpc.version1.id}"
  cidr_block        = "10.0.3.0/24"
  availability_zone = "us-east-1c"

  tags = {
    Name = "VPC-Subnet3"
  }
}

resource "aws_internet_gateway" "version1-igw" {
  vpc_id = "${aws_vpc.version1.id}"

  tags = {
    Name = "Version1 IGW"
  }
}

#Definition of route table
resource "aws_route_table" "version1-rt" {
  vpc_id = "${aws_vpc.version1.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.version1-igw.id}"
  }
  tags = {
    Name = "Version 1 Route Table - Public"
  }
}

#Assigning route table to subnet-a [considered as public subnet]
resource "aws_route_table_association" "version1-publicrt" {
  subnet_id      = "${aws_subnet.version1-subnet-a.id}"
  route_table_id = "${aws_route_table.version1-rt.id}"
}

#Creation of application load balancer referencing 3 subnets
resource "aws_lb" "version1-alb" {
  name               = "Version1-alb"
  internal           = false
  load_balancer_type = "application"
  subnets            = ["${aws_subnet.version1-subnet-a.id}", "${aws_subnet.version1-subnet-b.id}", "${aws_subnet.version1-subnet-c.id}"]
}
